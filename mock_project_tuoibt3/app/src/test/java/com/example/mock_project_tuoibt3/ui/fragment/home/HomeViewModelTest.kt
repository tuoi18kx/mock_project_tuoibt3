package com.example.mock_project_tuoibt3.ui.fragment.home

import com.example.mock_project_tuoibt3.domain.usecase.GetAllNewsUseCase
import com.example.mock_project_tuoibt3.domain.usecase.GetNewsByCategoryUseCase
import com.example.mock_project_tuoibt3.domain.usecase.SaveToDBUseCase
import io.mockk.mockk
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class HomeViewModelTest {

    private val getAllNewsUseCase: GetAllNewsUseCase = mockk()
    private val getNewsByCategoryUseCase: GetNewsByCategoryUseCase = mockk()
    private val saveToDBUseCase: SaveToDBUseCase = mockk()

    private lateinit var viewModel: HomeViewModel

    @Before
    fun setUp() {
        viewModel = HomeViewModel(getAllNewsUseCase, getNewsByCategoryUseCase, saveToDBUseCase)
    }

    @Test
    fun `givenCountry whenGetAllNews thenReturnData`(){
        //given
        val country = "us"

        viewModel.getAllNews(country)
    }

    /*@Test
    fun getListArticle() {
    }

    @Test
    fun getAllNews() {
    }

    @Test
    fun getNewsByCategory() {
    }*/
}