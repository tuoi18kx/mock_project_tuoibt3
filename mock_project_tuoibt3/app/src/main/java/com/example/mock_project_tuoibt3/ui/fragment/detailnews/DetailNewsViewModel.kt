package com.example.mock_project_tuoibt3.ui.fragment.detailnews

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mock_project_tuoibt3.data.mapper.transformToFirebase
import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.usecase.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.properties.Delegates

@HiltViewModel
class DetailNewsViewModel @Inject constructor(
    private val deleteNewsFromRealtimeDatabaseUseCase: DeleteNewsFromRealtimeDatabaseUseCase,
    private val insertNewsIntoRealtimeDatabaseUseCase: InsertNewsIntoRealtimeDatabaseUseCase,
    private val loginUseCase: LoginUseCase,
    private val saveToDBUseCase: SaveToDBUseCase
) : ViewModel() {

    private lateinit var newsFromFragment: Article

    var isConnected by Delegates.notNull<Boolean>()

    fun isLogedIn() = loginUseCase.isLogedIn()



    fun observerNetwork(isConnected: Boolean) {
        this.isConnected = isConnected
    }

    fun updateNewsToInsert(article: Article) {
        this.newsFromFragment = article
    }

    fun saveNewsToMarkList(article: Article) {
        viewModelScope.launch {
//            manipulateWithFavoriteListUseCase.addToFavoriteList(article)
        }
    }

    fun deleteNewsFromMarkList(article: Article) {
        viewModelScope.launch {
            try{
                saveToDBUseCase.insertNews(article, false)
            deleteNewsFromRealtimeDatabaseUseCase.deleteNewsMarkFromFirebase(article)
            }
            catch (e:Exception){e.printStackTrace()}
        }
    }

    fun insertMarkNewsIntoFirebase() {
        viewModelScope.launch {
            insertNewsIntoRealtimeDatabaseUseCase.addToFavoriteList(
                loginUseCase.currentUser!!.uid,
                newsFromFragment
            )
        }
    }
}
