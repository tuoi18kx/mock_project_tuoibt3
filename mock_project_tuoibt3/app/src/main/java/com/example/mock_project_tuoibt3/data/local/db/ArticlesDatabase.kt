package com.example.mock_project_tuoibt3.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.mock_project_tuoibt3.data.mapper.Converter
import com.example.mock_project_tuoibt3.data.local.dao.ArticlesDao
import com.example.mock_project_tuoibt3.data.model.Articles

@Database(entities = [Articles::class], version = 1,exportSchema = false)
@TypeConverters(Converter::class)
abstract class ArticlesDatabase: RoomDatabase() {
    abstract fun articlesDao(): ArticlesDao

}
