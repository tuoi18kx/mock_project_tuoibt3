package com.example.mock_project_tuoibt3.utils.imageloader

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.R
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class ImageLoaderImpl @Inject constructor(
    @ApplicationContext
    private val context: Context)
    : ImageLoader {
    override fun loadImg(urlToImage: String, imageView: ImageView) {
        Glide.with(context)
            .load(urlToImage)
            .placeholder(R.drawable.ic_splash_app)
            .into(imageView)

    }
}