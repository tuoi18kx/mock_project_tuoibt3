package com.example.mock_project_tuoibt3.utils.imageloader

import android.widget.ImageView

interface ImageLoader {
    fun loadImg(urlToImage: String, imageView: ImageView)
}