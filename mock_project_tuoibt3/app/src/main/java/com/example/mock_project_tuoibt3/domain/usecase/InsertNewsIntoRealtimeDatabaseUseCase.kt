package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import javax.inject.Inject

class InsertNewsIntoRealtimeDatabaseUseCase @Inject constructor(
    private val newsRepository: NewsRepository
)
{

    suspend fun addToFavoriteList(userID: String, article: Article){
        newsRepository.insertNewsMarkedIntoFirebase(userID, article)
//        newsRepository.insertNewsToDB(article, true)
    }
}