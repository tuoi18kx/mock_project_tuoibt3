package com.example.mock_project_tuoibt3.ui.fragment.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.mock_project_tuoibt3.databinding.LayoutProfileBinding
import com.example.mock_project_tuoibt3.gone
import com.example.mock_project_tuoibt3.visible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment:Fragment() {

    private lateinit var binding: LayoutProfileBinding
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser: FirebaseUser? = null
    private var isLogin: Boolean = false

    private val viewModel: ProfileViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUI()
        binding.txtProfileLogIn.setOnClickListener {
            val action = ProfileFragmentDirections.actionProfileFragment2ToLogInFragment2()
            findNavController().navigate(action)
        }

        binding.txtProfileLogOut.setOnClickListener {
            firebaseAuth.signOut()
            isLogin = false
            viewModel.deleteAll()
            setUI()
        }

        binding.txtProfileMarkedNews.setOnClickListener {
            val action = ProfileFragmentDirections.actionProfileFragment2ToMarkedNewsFragment()
            findNavController().navigate(action)
        }
    }
    override fun onStart() {
        super.onStart()
        currentUser = firebaseAuth.currentUser
        isLogin = currentUser!=null
    }

    override fun onResume() {
        super.onResume()
        setUI()
    }

    fun setUI(){
        if(isLogin == true){
            binding.txtProfileLogIn.isEnabled = false
            binding.txtProfileLogOut.visible()
            binding.txtProfileLogIn.text = currentUser!!.email.toString()
        }else{
            binding.txtProfileLogIn.isEnabled = true
            binding.txtProfileLogOut.gone()
            binding.txtProfileLogIn.text = "Log in"
        }
    }
}