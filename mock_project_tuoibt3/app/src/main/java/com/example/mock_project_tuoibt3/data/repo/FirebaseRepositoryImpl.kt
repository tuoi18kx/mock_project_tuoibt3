package com.example.mock_project_tuoibt3.data.repo

import com.example.mock_project_tuoibt3.domain.repo.FirebaseRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import javax.inject.Inject

class FirebaseRepositoryImpl @Inject constructor(
    private val firebaseAuth: FirebaseAuth
): FirebaseRepository{
    override val currentUser: FirebaseUser?
        get() = firebaseAuth.currentUser

    override suspend fun doLogin(email: String, pass: String){
        firebaseAuth.signInWithEmailAndPassword(email, pass)
    }

    override suspend fun doRegister(email: String, pass: String){
        firebaseAuth.createUserWithEmailAndPassword(email, pass)
    }

    override fun doLogout() {
        firebaseAuth.signOut()
    }
}