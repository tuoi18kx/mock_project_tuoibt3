package com.example.mock_project_tuoibt3.data.remote

import com.example.mock_project_tuoibt3.core.AppConfig.Companion.API_KEY1
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.API_KEY2
import com.example.mock_project_tuoibt3.data.model.Headlines
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    //cn: china, jp: japan, ru: russia, kr: korea, nz: New Zealand,

/*GET https://newsapi.org/v2/top-headlines?country=us&apiKey=9a9be645697e4fb08798a59b95dd033a*//*

    @GET("top-headlines")
    fun getTopHeadlines(@Query("country")country:String = "us", @Query("apiKey") apiKey:String = "9a9be645697e4fb08798a59b95dd033a"): Response<Headlines>
*/

    /*GET https://newsapi.org/v2/top-headlines?country=us&apiKey=9a9be645697e4fb08798a59b95dd033a*/
    @GET("top-headlines")
    fun getTopHeadlines(@Query("country")country:String = "us", @Query("apiKey") apiKey:String = API_KEY1): Single<Headlines>

    @GET("top-headlines")
    fun getTopicNews(@Query("country")country:String = "us", @Query("category") category: String = "business", @Query("apiKey") apiKey:String = API_KEY1): Single<Headlines>

    /*GET https://newsapi.org/v2/top-headlines?q=trump&apiKey=9a9be645697e4fb08798a59b95dd033a*/
    @GET("everything")
    fun search(@Query("q") keyWord:String, @Query("apiKey") apiKey:String = API_KEY1):  Single<Headlines>

    /*GET https://newsapi.org/v2/everything?domains=wsj.com&apiKey=API_KEY*/
}