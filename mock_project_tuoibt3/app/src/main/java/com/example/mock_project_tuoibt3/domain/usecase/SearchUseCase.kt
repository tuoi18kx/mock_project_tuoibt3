package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SearchUseCase @Inject constructor(private val newsRepository: NewsRepository) {
    fun searchNews(keyWord: String): Single<List<Article>>{
        return newsRepository.searchNews(keyWord)
    }

    fun searchNewsFromLocal(keyWord: String): List<Article>{
        return newsRepository.searchNewsFromLocal(keyWord)
    }
}