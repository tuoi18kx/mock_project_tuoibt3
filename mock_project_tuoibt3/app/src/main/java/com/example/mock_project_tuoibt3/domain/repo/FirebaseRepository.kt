package com.example.mock_project_tuoibt3.domain.repo

import com.google.firebase.auth.FirebaseUser

interface FirebaseRepository {
    val currentUser: FirebaseUser?

    suspend fun doLogin(email: String, pass:String)/*: Resource<FirebaseUser>*/

    suspend fun doRegister(email: String, pass: String)/* : Resource<FirebaseUser>*/

    fun doLogout()
}