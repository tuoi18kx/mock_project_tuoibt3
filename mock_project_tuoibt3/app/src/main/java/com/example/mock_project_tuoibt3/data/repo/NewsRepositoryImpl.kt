package com.example.mock_project_tuoibt3.data.repo

import android.util.Log
import android.widget.Toast
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.RTDB_REF_NAME
import com.example.mock_project_tuoibt3.data.local.dao.ArticlesDao
import com.example.mock_project_tuoibt3.data.mapper.transformFromFirebase
import com.example.mock_project_tuoibt3.data.mapper.transformTo
import com.example.mock_project_tuoibt3.data.mapper.transformToDB
import com.example.mock_project_tuoibt3.data.mapper.transformToFirebase
import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.data.remote.NewsApi
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.coroutineScope
import okhttp3.internal.threadName
import javax.inject.Inject
import kotlin.math.log

class NewsRepositoryImpl @Inject constructor(
    private val api: NewsApi,
    private val articlesDao: ArticlesDao,
    private val firebaseDatabase: FirebaseDatabase
) : NewsRepository {
    override fun getNewsCountry(country: String): Single<List<Article>> {
        return api.getTopHeadlines(country).map {
            it.articles.transformTo()
        }
    }

    override fun getNewsByCategory(country: String, category: String): Single<List<Article>> {
        return api.getTopicNews(country, category).map {
            it.articles.transformTo()
        }
    }

    override fun searchNews(keyWord: String): Single<List<Article>> {
        return api.search(keyWord).map {
            it.articles.transformTo()
        }
    }

    override suspend fun insertNewsToDB(article: Article, isFvr: Boolean) {
        articlesDao.insertArtiles(article.transformToDB(isFvr))
    }

    override suspend fun getNewsFavoriteLocal(): List<Article> {
        Log.d("getNewsFavoriteLocal", "getNewsFavoriteLocal: ${articlesDao.getArticlesFavorite(true).transformTo()}")
        return articlesDao.getArticlesFavorite(true).transformTo()
    }

    override suspend fun insertAllNewsToDB(listArticle: List<Article>, category: String) {
        articlesDao.insertListArticles(listArticle.transformToDB(category))
    }

    override suspend fun getNewsFromLocal(): List<Article> {
        return articlesDao.getAllArticles().transformTo()
    }

    override suspend fun getNewsByCategoryFromLocal(category: String): List<Article> {
        return articlesDao.getArticleByCategory(category).transformTo()
    }

    override fun searchNewsFromLocal(keyWord: String): List<Article> {
        return articlesDao.searchArticles(keyWord).transformTo()
    }

    override suspend fun insertNewsMarkedIntoFirebase(userMail: String, article: Article) {
        firebaseDatabase.getReference(RTDB_REF_NAME).child(userMail).child(article.title).setValue(article.transformToFirebase())
    }
    override suspend fun getNewsMarkedFromFirebase(userID: String): List<Article> {
        val listRs = ArrayList<ArticleFireBase>()
        val dataRef = firebaseDatabase.getReference(RTDB_REF_NAME).child(userID)
        dataRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for(dataSnapshot in snapshot.children){
                    val news = dataSnapshot.getValue(ArticleFireBase::class.java)
                    if (news != null) {
                        listRs.add(news)
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
        return listRs.toList().transformFromFirebase().transformTo()
    }

    override fun getDatabaseRef(userID: String): DatabaseReference{
        return firebaseDatabase.getReference(RTDB_REF_NAME).child(userID)
    }
    override fun deleteNewsMarkedFromFirebase(userID: String, article: ArticleFireBase) {
        firebaseDatabase.getReference(RTDB_REF_NAME).child(userID).child(article.title)
            .removeValue()
    }

    override suspend fun deleteAllLocal() {
        articlesDao.deleteAll()
    }
}