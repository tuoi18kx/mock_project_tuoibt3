package com.example.mock_project_tuoibt3

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view:View, position:Int)
}