package com.example.mock_project_tuoibt3.data.local.dao

import androidx.room.*
import com.example.mock_project_tuoibt3.data.model.Articles

@Dao
interface ArticlesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArtiles(articles: Articles)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertListArticles(listArticles: List<Articles>)

    @Query("select * from articles")
    fun getAllArticles(): List<Articles>

    @Delete
    fun deleteArticles(articles: Articles)
    @Query("select * from articles where category = :category")
    fun getArticleByCategory(category: String):List<Articles>

    @Query("select * from articles where category like '%' || :text || '%'")
    fun searchArticles(text: String): List<Articles>

    @Query("select * from articles where isFavor = :isFavor")
    fun getArticlesFavorite(isFavor: Boolean): List<Articles>

    @Query("DELETE FROM articles where isFavor = true")
    suspend fun deleteAll()
}