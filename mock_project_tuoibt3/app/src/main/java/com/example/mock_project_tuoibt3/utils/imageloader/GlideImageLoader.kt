package com.example.mock_project_tuoibt3.utils.imageloader

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import javax.inject.Inject

class GlideImageLoader @Inject constructor(private val context: Context) : ImageLoader {
    override fun loadImg(urlToImage: String, imageView: ImageView) {
        Glide.with(context).load(urlToImage).into(imageView)
    }
}