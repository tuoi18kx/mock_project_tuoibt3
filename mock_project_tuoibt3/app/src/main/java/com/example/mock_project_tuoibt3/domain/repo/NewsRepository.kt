package com.example.mock_project_tuoibt3.domain.repo

import com.example.mock_project_tuoibt3.core.AppConfig
import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.domain.model.Article
import com.google.firebase.database.DatabaseReference
import io.reactivex.rxjava3.core.Single

interface NewsRepository {
    fun getNewsCountry(country: String) : Single<List<Article>>
    fun getNewsByCategory(country: String,category: String) : Single<List<Article>>
    fun searchNews(keyWord:String): Single<List<Article>>
    suspend fun insertNewsToDB(article: Article, isFvr: Boolean)
    suspend fun getNewsFavoriteLocal(): List<Article>
    suspend fun insertAllNewsToDB(listArticle: List<Article>, category: String)
    suspend fun getNewsFromLocal(): List<Article>
    suspend fun getNewsByCategoryFromLocal(category: String): List<Article>
    fun searchNewsFromLocal(keyWord: String): List<Article>

    suspend fun insertNewsMarkedIntoFirebase(userMail: String, article: Article)

    suspend fun getNewsMarkedFromFirebase(userID: String): List<Article>

    fun deleteNewsMarkedFromFirebase(userID: String, article: ArticleFireBase)

    fun getDatabaseRef(userID: String): DatabaseReference

    suspend fun deleteAllLocal()
}