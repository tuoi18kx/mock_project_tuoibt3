package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.repo.FirebaseRepository
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import javax.inject.Inject

class LoginUseCase @Inject constructor(private val firebaseRepository: FirebaseRepository){

    val currentUser = firebaseRepository.currentUser

    fun isLogedIn(): Boolean{
        return firebaseRepository.currentUser != null
    }
}