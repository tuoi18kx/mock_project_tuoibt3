package com.example.mock_project_tuoibt3.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.mock_project_tuoibt3.*
import com.example.mock_project_tuoibt3.databinding.ActivityMainBinding
import com.example.mock_project_tuoibt3.utils.networkcheck.ConnectivityObserver
import com.example.mock_project_tuoibt3.utils.networkcheck.NetworkConnectivityObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var isConnected = false
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    lateinit var connectivityObserver: ConnectivityObserver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        connectivityObserver = NetworkConnectivityObserver(applicationContext)
        connectivityObserver.observerNetwork().onEach {
            Log.d("cjeclmet", "onViewCreated: ch al $it ")
            isConnected = it
        }.launchIn(lifecycleScope)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        navController = navHostFragment.navController

        binding.bottomNavigationView.setupWithNavController(navController)

        /*binding.bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.nav_news_feed -> replaceFragment(HomeFragment())
                R.id.nav_notification -> replaceFragment(NotificationFragment())
                R.id.nav_profile -> replaceFragment(ProfileFragment())
            }
            true
        }
*/
        binding.bottomNavigationView.setOnItemReselectedListener {
            navController.popBackStack(it.itemId, false)
        }

    }
    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.commit {
            replace(R.id.fragmentContainerView, fragment)
            setReorderingAllowed(true)
            /*setCustomAnimations(
                R.anim.enter_right_to_left,
                R.anim.exit_right_to_left,
                R.anim.enter_left_to_right,
                R.anim.exit_left_to_right
            )
            setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)*/
        }
    }

    fun showBottomBar() {
        binding.bottomNavigationView.visible()
    }
}