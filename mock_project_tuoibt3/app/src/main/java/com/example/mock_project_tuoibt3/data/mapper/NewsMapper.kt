package com.example.mock_project_tuoibt3.data.mapper

import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.data.model.Articles
import com.example.mock_project_tuoibt3.data.model.Sources
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.model.Source

fun List<Articles>.transformTo(): List<Article>{
    return this.map {
        Article(
            source = it.sources.transformTo(),
            author = it.author,
            title = it.title,
            description = it.description,
            url = it.url,
            urlToImage = it.urlToImage,
            publishedAt = it.publishedAt,
            content = it.content,
            category = it.category,
            isFavor = it.isFavor
        )
    }
}

fun List<Article>.transformToDB(category: String): List<Articles>{
    return this.map {
        Articles(
            sources = it.source.transformToModelDB(),
            author = it.author,
            title = it.title,
            description = it.description,
            url = it.url,
            urlToImage = it.urlToImage,
            publishedAt = it.publishedAt,
            content = it.content,
            category = category,
            isFavor = it.isFavor
        )
    }
}

fun Article.transformToFavorite(isFvr: Boolean): Articles{
    return Articles(
        sources = source.transformToModelDB(),
        author = author,
        title = title,
        description = description,
        url = url,
        urlToImage = urlToImage,
        publishedAt = publishedAt,
        content = content,
        category = category,
        isFavor = isFvr
    )
}
fun Article.transformToDB(isFvr: Boolean): Articles{
    return Articles(
        sources = source.transformToModelDB(),
        author = author,
        title = title,
        description = description,
        url = url,
        urlToImage = urlToImage,
        publishedAt = publishedAt,
        content = content,
        category = category,
        isFavor = isFvr
    )
}
fun Article.transformToFirebase(): ArticleFireBase{
    return ArticleFireBase(
        sources = source.name,
        author = author,
        title = title,
        description = description,
        url = url,
        urlToImage = urlToImage,
        publishedAt = publishedAt,
        content = content,
        category = category,
        isFavor = true
    )
}

fun ArticleFireBase.transformFromFirebase(): Article{
    return Article(
        source = Source(sources, sources),
        author = author,
        title = title,
        description = description,
        url = url,
        urlToImage = urlToImage,
        publishedAt = publishedAt,
        content = content,
        category = category,
        isFavor = true
    )
}


fun List<ArticleFireBase>.transformFromFirebase(): List<Articles>{
    return this.map {
        Articles(
            sources = Sources(it.sources, it.sources),
            author = it.author,
            title = it.title,
            description = it.description,
            url = it.url,
            urlToImage = it.urlToImage,
            publishedAt = it.publishedAt,
            content = it.content,
            category = it.category,
            isFavor = it.isFavor
        )
    }
}

fun Sources.transformTo(): Source{
    return Source(id = this.id, name = this.name)
}

fun Source.transformToModelDB(): Sources{
    return Sources(id = this.id, name = this.name)
}

