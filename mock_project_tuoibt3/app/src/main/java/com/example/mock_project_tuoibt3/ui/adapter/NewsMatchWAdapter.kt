package com.example.mock_project_tuoibt3.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.OnItemClickListener
import com.example.mock_project_tuoibt3.R
import com.example.mock_project_tuoibt3.databinding.ItemNewsMatchWidthBinding
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.utils.imageloader.ImageLoader
import dagger.hilt.android.AndroidEntryPoint
import java.security.PrivateKey
import javax.inject.Inject

class NewsMatchWAdapter @Inject constructor(private val imageLoader: ImageLoader,
                                            private val listNews: List<Article>,
                                            private val listener: OnItemClickListener
) :
    RecyclerView.Adapter<NewsMatchWAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding =
            ItemNewsMatchWidthBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listNews.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
//        Glide.with(holder.binding.root).load(listNews[position].urlToImage)
//            .into(holder.binding.imvNewsThumbnail)
        imageLoader.loadImg(listNews[position].urlToImage.toString(), holder.binding.imvNewsThumbnail)
        holder.binding.txtNewsTitle.text = listNews[position].title
        holder.onClickItem(listener, position)
    }

    inner class NewsViewHolder(val binding: ItemNewsMatchWidthBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onClickItem(listener: OnItemClickListener, position: Int) {
            itemView.setOnClickListener {
                listener.onItemClick(itemView, position)
            }
        }
    }
}