package com.example.mock_project_tuoibt3.ui.fragment.search

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.usecase.GetNewsByCategoryUseCase
import com.example.mock_project_tuoibt3.domain.usecase.ManipulateWithFavoriteListUseCase
import com.example.mock_project_tuoibt3.domain.usecase.SaveToDBUseCase
import com.example.mock_project_tuoibt3.domain.usecase.SearchUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.properties.Delegates

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val getNewsByCategoryUseCase: GetNewsByCategoryUseCase,
    private val searchUseCase: SearchUseCase,
    private val saveToDBUseCase: SaveToDBUseCase,
    private val manipulateWithFavoriteListUseCase: ManipulateWithFavoriteListUseCase,
) : ViewModel() {
    var listNewsSearch = MutableLiveData<List<Article>>()
    var listArticleByCategory = MutableLiveData<List<Article>>()

    var isConnected by Delegates.notNull<Boolean>()

    fun observerNetwork(isConnected: Boolean){
        this.isConnected = isConnected
    }
    fun getNewsSearch(keyWord: String) {
        viewModelScope.launch {
            if (isConnected == true) {
                searchUseCase.searchNews(keyWord).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeBy(onSuccess = {
                        listNewsSearch.value = it
                    }, onError = {
                    })
            } else {
                listNewsSearch.value = searchUseCase.searchNewsFromLocal(keyWord)
            }
        }
    }
    fun getNewsByCategory(country: String, category: String) {
        if (isConnected) {
            viewModelScope.launch {
                getNewsByCategoryUseCase.getNewsByCategory(country, category)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(onSuccess = {
                        listArticleByCategory.value = it
                        insertListIntoRoom(it, category)
                    }, onError = {
                        Log.d("bnj", "fail: $it")
                    })
            }
        } else {
            viewModelScope.launch {
                listArticleByCategory.value =
                    getNewsByCategoryUseCase.getNewsByCategoryFromLocal(category)
            }
        }
    }

    private fun insertListIntoRoom(list: List<Article>, category: String) {
        viewModelScope.launch {
            saveToDBUseCase.insertAll(list,category)
        }
    }
    fun getNewsFavorite() {
        viewModelScope.launch {
            listArticleByCategory.value = manipulateWithFavoriteListUseCase.getListFavorite()
        }
    }
}
