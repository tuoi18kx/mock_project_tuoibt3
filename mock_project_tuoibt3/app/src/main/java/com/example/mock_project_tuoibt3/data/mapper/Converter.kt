package com.example.mock_project_tuoibt3.data.mapper

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import androidx.room.TypeConverter
import com.example.mock_project_tuoibt3.data.model.Sources
import java.io.ByteArrayOutputStream

class Converter {

    @TypeConverter
    fun fromSources(sources: Sources):String{
        return sources.name
    }

    @TypeConverter
    fun toSources(name:String): Sources{
        return Sources(name, name)
    }

}