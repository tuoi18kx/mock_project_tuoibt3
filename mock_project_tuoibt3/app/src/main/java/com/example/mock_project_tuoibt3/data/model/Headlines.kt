package com.example.mock_project_tuoibt3.data.model

import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Headlines (
    @PrimaryKey
    @SerializedName("status")
    @Expose
    var status: String = "",

    @SerializedName("totalResults")
    @Expose
    var totalResults: String = "",

    @SerializedName("articles")
    @Expose
    var articles: List<Articles> = emptyList()
)