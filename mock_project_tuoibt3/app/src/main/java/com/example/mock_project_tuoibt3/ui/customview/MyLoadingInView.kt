package com.example.mock_project_tuoibt3.ui.customview

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.example.mock_project_tuoibt3.R

class MyLoadingInView(context: Context?, attrs: AttributeSet?) : View(context, attrs){
    private val paint = Paint()

    @Volatile
    private var progress: Double = 0.0
    private var valueAnimator: ValueAnimator
    private val updateListener = ValueAnimator.AnimatorUpdateListener {
        progress = (it.animatedValue as Float).toDouble()
        invalidate()
        requestLayout()
    }

    init {
        valueAnimator = AnimatorInflater.loadAnimator(
            context,
            R.animator.loading_animation
        ) as ValueAnimator
        valueAnimator.addUpdateListener(updateListener)
        valueAnimator.start()
    }


    private var w = 320
    private var h = 320

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        w = measuredWidth
        h = measuredHeight
        Log.d("splashview", "onMeasure: $w, $h")
        setMeasuredDimension(w, h)
    }
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.style = Paint.Style.FILL
        paint.color = Color.YELLOW
        paint.strokeWidth = h*1f

        canvas.drawLine(0f*w, 0.3f*h, progress.toFloat()*1f*w/100, 0.3f*h, paint)
    }

}