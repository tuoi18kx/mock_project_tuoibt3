package com.example.mock_project_tuoibt3.ui.fragment.marked

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mock_project_tuoibt3.OnItemClickListener
import com.example.mock_project_tuoibt3.databinding.LayoutSearchBinding
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.gone
import com.example.mock_project_tuoibt3.utils.imageloader.ImageLoader
import com.example.mock_project_tuoibt3.ui.adapter.NewsMatchWAdapter
import com.example.mock_project_tuoibt3.visible
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MarkedNewsFragment: Fragment() {
    private lateinit var binding: LayoutSearchBinding
    private val viewModel: MarkedNewsViewModel by viewModels()
    @Inject
    lateinit var imgLoader: ImageLoader
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LayoutSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView() {
        binding.lnSearch.gone()
        viewModel.getNewsFavorite()
        viewModel.listNewsMarked.observe(viewLifecycleOwner){
            if(it.isEmpty()){
                binding.txtEmpty.visible()
                binding.rcvSearchNewsList.gone()
            }
            else{
                setRcv(it)
            }
        }
    }
    private fun setRcv(listNews: List<Article>) {
        binding.txtEmpty.gone()
        binding.rcvSearchNewsList.visible()
        binding.rcvSearchNewsList.adapter = NewsMatchWAdapter(imgLoader, listNews, object : OnItemClickListener {
            override fun onItemClick(view: View, position: Int) {
                val action = MarkedNewsFragmentDirections.actionMarkedNewsFragmentToArticlesWebViewFragment3(listNews[position])
                findNavController().navigate(action)
            }
        })
    }
}