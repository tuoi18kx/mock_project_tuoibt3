package com.example.mock_project_tuoibt3.ui.fragment.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mock_project_tuoibt3.domain.usecase.RegisterUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.properties.Delegates

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val registerUseCase: RegisterUseCase
    ) : ViewModel() {

    var isRegisterSuccess by Delegates.notNull<Boolean>()

    fun doRegister(email: String, pass:String){
        viewModelScope.launch {
            registerUseCase.doRegister(email, pass)
        }
    }

    /*private val _loginFlow = MutableStateFlow<Resource<FirebaseUser>?>(null)
    val loginFlow: StateFlow<Resource<FirebaseUser>?> = _loginFlow

    private val _signupFlow = MutableStateFlow<Resource<FirebaseUser>?>(null)
    val signupFlow: StateFlow<Resource<FirebaseUser>?> = _signupFlow

    val currentUser: FirebaseUser?
        get() = firebaseRepository.currentUser

    init {
        if (firebaseRepository.currentUser != null) {
            _loginFlow.value = Resource.Success(firebaseRepository.currentUser!!)
        }
    }

    fun loginUser(email: String, password: String) = viewModelScope.launch {
        _loginFlow.value = Resource.Loading
        val result = firebaseRepository.doLogin(email, password)
        _loginFlow.value = result
    }

    fun signupUser(name: String, email: String, password: String) = viewModelScope.launch {
        _signupFlow.value = Resource.Loading
        val result = firebaseRepository.doRegister(email, password)
        _signupFlow.value = result
    }

    fun logout() {
        firebaseRepository.doLogout()
        _loginFlow.value = null
        _signupFlow.value = null
    }*/
}