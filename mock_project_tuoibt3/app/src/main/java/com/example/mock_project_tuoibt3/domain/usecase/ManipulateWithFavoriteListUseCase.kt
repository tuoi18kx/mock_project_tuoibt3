package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import javax.inject.Inject

class ManipulateWithFavoriteListUseCase @Inject constructor(private val newsRepository: NewsRepository) {
    suspend fun addToFavoriteList(article: Article){
        newsRepository.insertNewsToDB(article, true)
    }

    suspend fun getListFavorite(): List<Article>{
        return newsRepository.getNewsFavoriteLocal()
    }

    suspend fun deleteFromFavoriteList(article: Article){
        newsRepository.insertNewsToDB(article ,isFvr = false)
    }

}