package com.example.mock_project_tuoibt3.core

import com.example.mock_project_tuoibt3.domain.model.Article

interface AppConfig{
    companion object {
        const val API_KEY1 = "9a9be645697e4fb08798a59b95dd033a"
        const val API_KEY2 = "7b8c0c5f7ce640c19130edcc5d72d867"
        const val BASE_URL = "https://newsapi.org/v2/"
        const val BUSINESS = "business"
        const val ENTERTAINMENT = "entertainment"
        const val HEALTH = "health"
        const val SCIENCE = "science"
        const val SPORTS = "sports"
        const val TECHNOLOGY = "technology"
        const val GENERAL = "general"
        const val CHANNEL_ID = "channelID"
        const val CHANNEL_NAME = "com.example.mock_project"
        const val RTDB_REF_NAME = "SuperVipNewsApp"
    }
}