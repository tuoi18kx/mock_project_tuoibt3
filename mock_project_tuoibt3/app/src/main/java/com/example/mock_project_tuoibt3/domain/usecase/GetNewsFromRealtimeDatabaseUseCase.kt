package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.data.mapper.transformFromFirebase
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.FirebaseRepository
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import com.google.firebase.database.DatabaseReference
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class GetNewsFromRealtimeDatabaseUseCase @Inject constructor(
    private val newsRepository: NewsRepository,
    private val firebaseRepository: FirebaseRepository
    ) {

    suspend fun getListFavorite(): List<Article>{
//        return newsRepository.getNewsFavoriteLocal()
        return newsRepository.getNewsMarkedFromFirebase(firebaseRepository.currentUser!!.uid)
    }

    fun getDataRef(userID: String): DatabaseReference{
        return newsRepository.getDatabaseRef(userID)
    }
}