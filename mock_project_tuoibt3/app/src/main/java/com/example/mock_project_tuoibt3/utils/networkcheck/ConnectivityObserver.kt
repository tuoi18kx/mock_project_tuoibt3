package com.example.mock_project_tuoibt3.utils.networkcheck

import kotlinx.coroutines.flow.Flow


interface ConnectivityObserver {
    fun observerNetwork(): Flow<Boolean>
}