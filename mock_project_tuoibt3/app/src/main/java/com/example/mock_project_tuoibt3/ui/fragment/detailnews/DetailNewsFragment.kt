package com.example.mock_project_tuoibt3.ui.fragment.detailnews

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.notif.ForegroundService
import com.example.mock_project_tuoibt3.R
import com.example.mock_project_tuoibt3.databinding.LayoutDetailNewsBinding
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.utils.imageloader.ImageLoader
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.properties.Delegates

@AndroidEntryPoint
class DetailNewsFragment(
) : Fragment() {
    private val viewModel: DetailNewsViewModel by viewModels()
    private lateinit var binding: LayoutDetailNewsBinding
    private val args: DetailNewsFragmentArgs by navArgs()
    private var isFavorite by Delegates.notNull<Boolean>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutDetailNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUI()
    }

    private fun setUI() {
        val news = args.article
        isFavorite = news.isFavor
        viewModel.updateNewsToInsert(news)
        val isLogedIn = viewModel.isLogedIn()


        with(binding){
            txtDetailTitle.text = news.title
            txtDetailAuthor.text = news.author
            txtDetailContent.text = news.content
            Glide.with(root).load(news.urlToImage.toString()).into(imvDetailImage)
            txtDetailDescription.text = news.description

            setHeart(isFavorite)
            imvHeart.setOnClickListener {
                try{
                    if(isLogedIn == true){
                        doFvrList(news)
                    }
                    else{
                        val action = DetailNewsFragmentDirections.actionArticlesWebViewFragment3ToLogInFragment2()
                        findNavController().navigate(action)
                    }
                }
                catch (e:Exception){
                    e.printStackTrace()
                }
            }
        }
    }

    private fun setHeart(isfavorite: Boolean) {
        if(isfavorite){
            Glide.with(this).load(R.drawable.img_hearted).into(binding.imvHeart)
        }else{
            Glide.with(this).load(R.drawable.img_heart).into(binding.imvHeart)
        }
    }

    private fun doFvrList(news: Article) {
        isFavorite = if(isFavorite){
            viewModel.deleteNewsFromMarkList(news)
            val intent = Intent(context, ForegroundService::class.java).apply {
                putExtra("TITLE_NEWS", "title news here")
            }
            Glide.with(this).load(R.drawable.img_heart).into(binding.imvHeart)
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
                requireContext().startForegroundService(intent)
            } else {
                requireContext().startService(intent)
            }
            requireContext().startService(intent)
            intent.putExtra("TITLE_NEWS", news.title)

            false
        }else{
            viewModel.insertMarkNewsIntoFirebase()
            Glide.with(this).load(R.drawable.img_hearted).into(binding.imvHeart)
            true
        }
    }
}