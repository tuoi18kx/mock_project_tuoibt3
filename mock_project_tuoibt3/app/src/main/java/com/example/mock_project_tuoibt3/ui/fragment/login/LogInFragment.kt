package com.example.mock_project_tuoibt3.ui.fragment.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mock_project_tuoibt3.databinding.LayoutLogInBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlin.concurrent.thread

@AndroidEntryPoint
class LogInFragment : Fragment() {
    private lateinit var binding: LayoutLogInBinding
    private val viewModel: LogInViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutLogInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.txtDontHaveAccount.setOnClickListener {
            val action = LogInFragmentDirections.actionLogInFragment2ToRegisterFragment2()
            findNavController().navigate(action)
        }

        binding.btnLogIn.setOnClickListener {
            val email = binding.txtSignInUsername.text.toString()
            val pass = binding.txtSignInPassword.text.toString()
            if (email.isEmpty() || pass.isEmpty()) {
                Toast.makeText(requireContext(), "Cannot leave blank!", Toast.LENGTH_SHORT).show()
            } else {
                viewModel.doLogin(email, pass)
                viewModel.isLoginSuccess.observe(viewLifecycleOwner){
                    if(it==true){
                        val action = LogInFragmentDirections.actionLogInFragment2ToProfileFragment2()
                        findNavController().navigate(action)
                    }
                    else{
                        Toast.makeText(requireContext(), "something wrong", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    }
}