package com.example.mock_project_tuoibt3.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import javax.inject.Inject
import kotlin.system.measureTimeMillis

const val TAG = "MainViewModel"

@HiltViewModel
class MainViewModel @Inject constructor(
    private val newsRepository: NewsRepository
): ViewModel() {

    var listArticle = MutableLiveData<List<Article>>()
    var listArticleByCategory= MutableLiveData<List<Article>>()
    var listArticleFromDB = MutableLiveData<List<Article>>()

    /*@SuppressLint("CheckResult")
    fun getAllNewsAndInsertIntoDB(context: Context) {
        viewModelScope.launch {
            val elapsedTime= measureTimeMillis {
                newsRepository.getNewsCountry("us")
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribeBy(
                     onSuccess = {
                         listArticle.value = it
                     },
                     onError = {
                         Log.d("bnj", "fail: $it")
                     }
                 )
            }
            *//*newsRepository.getNewsCountry("us",100)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        listArticle.value = it
                        insertListIntoRoom(context, it)
                    },
                    onError = {
                        Log.d("bnj", "fail: $it")
                    }
                )*//*
        }
    }
    fun insertListIntoRoom(context: Context, list: List<Article>) {
        for(item in list){
            writeToDB_bitmap(context, item)
        }
    }

    @SuppressLint("CheckResult")
    fun getNewsByCategoryAndInsertToDB(context: Context, category: String){
        val time = measureTimeMillis {
                newsRepository.getNewsByCategory("us", category)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onSuccess = {
                            val arr = it.map {
                                Article(source = it.source,
                                    author = it.author,
                                    title = it.title,
                                    description = it.description,
                                    url = it.url,
                                    urlToImage = it.urlToImage,
                                    publishedAt = it.publishedAt,
                                    content = it.content,
                                    category = category)
                            }
                            listArticleByCategory.value = arr
                            insertListIntoRoom(context, arr)
                        },
                        onError = {
                            Log.d("bnj", "fail: $it")
                        }
                    )
        }
        Log.d(TAG, "getNewsByCategoryAndInsertToDB: $time complete")
    }

    fun getAllNewFromDB(){
        listArticleFromDB.value = newsRepository.getNewsFromLocal()
        Log.d("bnj", "getAllNewFromDB: ${newsRepository.getNewsFromLocal()}")
    }


    suspend fun getBitmap(context: Context, url: String?): Bitmap?{
        var bitmap: Bitmap? = null
        try{
            bitmap = withContext(Dispatchers.IO) {
                Glide.with(context).asBitmap().load(url).submit().get()
            }
        } catch (e:Exception){
            e.printStackTrace()
        }
        return bitmap
    }

    fun writeToDB_bitmap(context: Context, article: Article){
        viewModelScope.launch{
            newsRepository.insertNewsToDB(transformFromBitmap(article, context))
        }
    }

    private suspend fun transformFromBitmap(article: Article, context: Context): Article {
        return Article(article.source, article.author, article.title, article.description, article.url,
            fromBitmap(getBitmap(context, article.urlToImage)), article.publishedAt, article.content, article.category)
    }

    fun fromBitmap(bitmap: Bitmap?): String {
        val outputStream = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
        val b = outputStream.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }*/
}