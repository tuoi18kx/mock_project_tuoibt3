package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.data.mapper.transformToFirebase
import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.FirebaseRepository
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import javax.inject.Inject

class DeleteNewsFromRealtimeDatabaseUseCase @Inject constructor(
    private val newsRepository: NewsRepository,
    private val firebaseRepository: FirebaseRepository
)
{
    fun deleteNewsMarkFromFirebase(article: Article){
        newsRepository.deleteNewsMarkedFromFirebase(firebaseRepository.currentUser!!.uid, article.transformToFirebase())

    }

   suspend fun deleteNewsMarkLocal(article: Article){
        newsRepository.insertNewsToDB(article, false)
    }
}