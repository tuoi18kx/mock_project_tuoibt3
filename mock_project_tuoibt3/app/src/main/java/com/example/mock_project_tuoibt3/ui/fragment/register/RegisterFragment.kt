package com.example.mock_project_tuoibt3.ui.fragment.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mock_project_tuoibt3.databinding.LayoutRegisterBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment() {
    private lateinit var binding: LayoutRegisterBinding
    private val viewModel: RegisterViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = LayoutRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnRegister.setOnClickListener { doRegister() }
        binding.txtLogIn.setOnClickListener {
            val action = RegisterFragmentDirections.actionRegisterFragment2ToLogInFragment2()
            findNavController().navigate(action)
        }
    }

    private fun doRegister() {
        val email = binding.edtSignUpEmail.text.toString()
        val pass = binding.edtSignUpPassword.text.toString()
        val confirmPass = binding.edtSignUpConfirmPassword.text.toString()

        if (email.isEmpty() || pass.isEmpty() || confirmPass.isEmpty()) {
            Toast.makeText(requireContext(), "Cannot leave blank!", Toast.LENGTH_SHORT).show()
        } else {
            if (pass == confirmPass) {
                viewModel.doRegister(email, pass)
                if (viewModel.isRegisterSuccess == true) {
                    val action = RegisterFragmentDirections.actionRegisterFragment2ToLogInFragment2()
                    findNavController().navigate(action)
                } else {
                    Toast.makeText(requireContext(), "something wrong", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(
                    requireContext(), "Password is not matching!", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}