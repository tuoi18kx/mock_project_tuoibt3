package com.example.mock_project_tuoibt3.ui.fragment.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mock_project_tuoibt3.domain.usecase.SaveToDBUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val saveToDBUseCase: SaveToDBUseCase,
) : ViewModel() {

    fun deleteAll(){
        viewModelScope.launch {
            saveToDBUseCase.deleteAll()
        }
    }
}