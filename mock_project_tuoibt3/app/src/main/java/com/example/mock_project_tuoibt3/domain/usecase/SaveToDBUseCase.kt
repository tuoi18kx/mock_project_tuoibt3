package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import javax.inject.Inject

class SaveToDBUseCase @Inject constructor(private val newsRepository: NewsRepository) {
    suspend fun insertAll(listArticle: List<Article>, category: String) {
        newsRepository.insertAllNewsToDB(listArticle, category)
    }
    suspend fun insertNews(article: Article, isFavor: Boolean){
        newsRepository.insertNewsToDB(article, isFavor)
    }

    suspend fun deleteAll(){
        newsRepository.deleteAllLocal()
    }
}