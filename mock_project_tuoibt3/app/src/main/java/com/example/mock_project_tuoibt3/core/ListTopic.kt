package com.example.mock_project_tuoibt3.core

import com.example.mock_project_tuoibt3.R
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.BUSINESS
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.ENTERTAINMENT
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.HEALTH
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.SCIENCE
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.SPORTS
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.TECHNOLOGY

class Topic(val name:String, val img: Int)
val listTopic = listOf(
    Topic(BUSINESS, R.drawable.img_business),
    Topic(HEALTH, R.drawable.img_health),
    Topic(SPORTS, R.drawable.img_sport),
    Topic(ENTERTAINMENT, R.drawable.img_entertainment),
    Topic(TECHNOLOGY, R.drawable.img_technology),
    Topic(SCIENCE, R.drawable.img_science)
)