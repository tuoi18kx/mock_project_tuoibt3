package com.example.mock_project_tuoibt3.ui.fragment.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mock_project_tuoibt3.data.mapper.transformFromFirebase
import com.example.mock_project_tuoibt3.data.mapper.transformToDB
import com.example.mock_project_tuoibt3.data.model.ArticleFireBase
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.usecase.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getAllNewsUseCase: GetAllNewsUseCase,
    private val getNewsByCategoryUseCase: GetNewsByCategoryUseCase,
    private val saveToDBUseCase: SaveToDBUseCase,
    private val getNewsFromRealtimeDatabaseUseCase: GetNewsFromRealtimeDatabaseUseCase,
    private val loginUseCase: LoginUseCase,
    private val manipulateWithFavoriteListUseCase: ManipulateWithFavoriteListUseCase
) : ViewModel() {
    var listArticle = MutableLiveData<List<Article>>()
    var listArticleByCategory = MutableLiveData<List<Article>>()

    var listMarkedNewsFromFirebase = MutableLiveData<List<Article>>()

    private var isConnected = false
    val currentUser = loginUseCase.currentUser

    fun observerNetwork(isConnected: Boolean) {
        this.isConnected = isConnected
    }


    fun getAllNews(country: String) {
        viewModelScope.launch {
            Log.d("mmnj", "getAllNews: $isConnected")
            if(isConnected == true){
                getAllNewsUseCase.getNewsCountry(country).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeBy(
                        onSuccess = {
                            listArticle.value = it
                            insertListIntoRoom(it, it[0].category.toString())
                        },
                        onError = {})
            }
            else{
                Log.d("mmnj", "getAllNews: ${getAllNewsUseCase.getNewsFromLocal()}")
                listArticle.value = getAllNewsUseCase.getNewsFromLocal()
            }
        }
    }

    fun getNewsByCategory(country: String, category: String) {
        viewModelScope.launch {
            Log.d("vnj", "getNewsByCategory: $isConnected")
            if(isConnected){
                getNewsByCategoryUseCase.getNewsByCategory(country, category)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onSuccess = {
                            listArticleByCategory.value = it
                            insertListIntoRoom(it, category)
                        },
                        onError = {})
            }else{
                listArticleByCategory.value =
                    getNewsByCategoryUseCase.getNewsByCategoryFromLocal(category)
            }
        }
    }
    private fun insertListIntoRoom(list: List<Article>, category: String) {
        viewModelScope.launch {
            saveToDBUseCase.insertAll(list, category)
        }
    }

    private fun insertArticleIntoRoom(article: Article, isFavor: Boolean){
        viewModelScope.launch {
            Log.d("insert mark", "insertArticleIntoRoom: $article")
            saveToDBUseCase.insertNews(article, isFavor)
        }
    }

    fun getMarkedListAndInsertIntoDB(userID: String){
        if(isConnected){
            viewModelScope.launch {
                val listNews = ArrayList<Article>()
                getNewsFromRealtimeDatabaseUseCase.getDataRef(userID).addValueEventListener(object :
                    ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for(dataSnapshot in snapshot.children){
                            val news = dataSnapshot.getValue(ArticleFireBase::class.java)
                            if (news != null) {
                                listNews.add(news.transformFromFirebase())
                                insertArticleIntoRoom(news.transformFromFirebase(), true)
                            }
                        }
                        listMarkedNewsFromFirebase.value = listNews
                        Log.d("homeview", "onDataChange: $listNews")
                    }
                    override fun onCancelled(error: DatabaseError) {
                    }
                })
            }
        }
    }

    fun getLocalFavor(){
        viewModelScope.launch {
            Log.d("bnk", "getLocalFavor: ${manipulateWithFavoriteListUseCase.getListFavorite().size}")

        }
    }
}