package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetNewsByCategoryUseCase  @Inject constructor(private val newsRepository: NewsRepository) {
    fun getNewsByCategory(country: String, category: String): Single<List<Article>> {
        return newsRepository.getNewsByCategory(country, category)
    }

    suspend fun getNewsByCategoryFromLocal(category: String): List<Article> {
        return newsRepository.getNewsByCategoryFromLocal(category)
    }
}