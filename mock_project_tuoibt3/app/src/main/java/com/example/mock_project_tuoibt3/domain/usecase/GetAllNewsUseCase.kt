package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetAllNewsUseCase @Inject constructor(private val newsRepository: NewsRepository) {
    fun getNewsCountry(country: String): Single<List<Article>> {
        return newsRepository.getNewsCountry(country)
    }

    suspend fun getNewsFromLocal(): List<Article> {
        return newsRepository.getNewsFromLocal()
    }
}