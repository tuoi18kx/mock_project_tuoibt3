package com.example.mock_project_tuoibt3.data.model

import androidx.room.PrimaryKey
import com.example.mock_project_tuoibt3.core.AppConfig

data class ArticleFireBase(
    var sources: String = "",
    var author: String? = "",
    @PrimaryKey
    var title: String = "",
    var description: String? = "",
    var url: String? = "",
    var urlToImage: String? = "",
    var publishedAt: String? = "",
    var content: String? = "",
    var category: String? = "",
    var isFavor: Boolean = false
)