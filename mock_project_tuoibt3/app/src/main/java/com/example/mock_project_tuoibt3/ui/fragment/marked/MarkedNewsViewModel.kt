package com.example.mock_project_tuoibt3.ui.fragment.marked

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mock_project_tuoibt3.domain.model.Article
import com.example.mock_project_tuoibt3.domain.usecase.GetNewsFromRealtimeDatabaseUseCase
import com.example.mock_project_tuoibt3.domain.usecase.ManipulateWithFavoriteListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MarkedNewsViewModel @Inject constructor(
    private val manipulateWithFavoriteListUseCase: ManipulateWithFavoriteListUseCase,
    private val getNewsFromRealtimeDatabaseUseCase: GetNewsFromRealtimeDatabaseUseCase
) : ViewModel() {

    var listNewsMarked = MutableLiveData<List<Article>>()

    fun getNewsFavorite() {
        viewModelScope.launch {
            listNewsMarked.value = manipulateWithFavoriteListUseCase.getListFavorite()
//            listNewsMarked.value = getNewsFromRealtimeDatabaseUseCase.getListFavorite()
        }
    }

    fun deleteNewsFromFavorite(article: Article){
        viewModelScope.launch {
            manipulateWithFavoriteListUseCase.deleteFromFavoriteList(article)
        }
    }
}
