package com.example.mock_project_tuoibt3.ui.fragment.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.mock_project_tuoibt3.OnItemClickListener
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.BUSINESS
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.GENERAL
import com.example.mock_project_tuoibt3.core.listTopic
import com.example.mock_project_tuoibt3.databinding.LayoutHomeBinding
import com.example.mock_project_tuoibt3.utils.imageloader.GlideImageLoader
import com.example.mock_project_tuoibt3.utils.imageloader.ImageLoader
import com.example.mock_project_tuoibt3.ui.adapter.NewsMatchWAdapter
import com.example.mock_project_tuoibt3.ui.adapter.NewsSmallAdapter
import com.example.mock_project_tuoibt3.ui.adapter.TopicAdapter
import com.example.mock_project_tuoibt3.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment(
) : Fragment() {
    private lateinit var binding: LayoutHomeBinding
    private val viewModel: HomeViewModel by viewModels()
    private val country = "us"
    @Inject
    lateinit var imgLoader: ImageLoader

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).showBottomBar()

        viewModel.observerNetwork((activity as MainActivity).isConnected)
        viewModel.getAllNews(country)
        viewModel.getNewsByCategory(country, BUSINESS)
        if(viewModel.currentUser != null){
            Log.d("bnjj", "onViewCreated: arlch")
            viewModel.getMarkedListAndInsertIntoDB(viewModel.currentUser!!.uid)
            viewModel.getLocalFavor()
        }
        initViews()
    }

    private fun initViews() {
        setUI()
        binding.imvSearch.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragment3ToSearchFragment2(true, "")
            findNavController().navigate(action)
        }

        binding.txtGeneralNewSeeMore.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragment3ToSearchFragment2(false, GENERAL)
            findNavController().navigate(action)
        }
    }

    private fun setUI() {
        //list topic
        binding.rcvHomeCategory.adapter =
            TopicAdapter(requireContext(), listTopic, object : OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    val action = HomeFragmentDirections.actionHomeFragment3ToSearchFragment2(
                        false,
                        listTopic[position].name
                    )
                    findNavController().navigate(action)
                }
            })

        //small
        viewModel.listArticle.observe(viewLifecycleOwner) {
            binding.rcvHomeNewsSmall.adapter =
                NewsSmallAdapter(it, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val action =
                            HomeFragmentDirections.actionHomeFragment3ToArticlesWebViewFragment3(it[position])
                        findNavController().navigate(action)
                    }
                })
        }
        //match
        viewModel.listArticleByCategory.observe(viewLifecycleOwner) {
            binding.rcvHomeNewsMatchWidth.adapter =
                NewsMatchWAdapter(imgLoader,it, object : OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val action =
                            HomeFragmentDirections.actionHomeFragment3ToArticlesWebViewFragment3(it[position])
                        findNavController().navigate(action)
                    }
                })
        }
    }
}