package com.example.mock_project_tuoibt3.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.mock_project_tuoibt3.core.AppConfig.Companion.GENERAL

@Entity(tableName = "articles")
data class Articles(
    /*@PrimaryKey
    @SerializedName("title")
    @Expose
    var title: String = "",

    @SerializedName("author")
    @Expose
    var author: String? = "",

    @SerializedName("description")
    @Expose
    var description: String? = "",

    @SerializedName("url")
    @Expose
    var url: String? = "",

    @SerializedName("urlToImage")
    @Expose
    var urlToImage: String? = "",

    @SerializedName("publishedAt")
    @Expose
    var publishedAt: String? = "",

    @SerializedName("content")
    @Expose
    var content: String? = "",

    *//*@SerializedName("sources")
    @Expose
    var sources: Sources = Sources("", "")*/

    var sources: Sources = Sources(),
    var author: String? = "",
    @PrimaryKey
    var title: String = "",
    var description: String? = "",
    var url: String? = "",
    var urlToImage: String? = "",
    var publishedAt: String? = "",
    var content: String? = "",
    var category: String? = GENERAL,
    var isFavor: Boolean = false
)