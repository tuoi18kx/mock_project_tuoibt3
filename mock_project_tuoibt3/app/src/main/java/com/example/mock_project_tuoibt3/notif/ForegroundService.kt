package com.example.mock_project_tuoibt3.notif

import android.app.*
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.mock_project_tuoibt3.R
import okhttp3.internal.notify

class ForegroundService : Service() {

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val text = intent.getStringExtra("TITLE_NEWS")
        sendNotification(text.toString())
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun sendNotification(text: String) {
        Log.d("foreground sv", "sendNotification: ch")
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this,)

        Log.d("service_app", "sendNotification: ardch")
        notificationBuilder.setSmallIcon(R.drawable.ic_launcher_foreground)
        notificationBuilder.setContentTitle("SuperVipPro NewsApp")
        notificationBuilder.setContentText("Add to mark list successfully!\n$text")
        notificationBuilder.priority = NotificationCompat.PRIORITY_HIGH

        startForeground(101, notificationBuilder.build())
    }

    /*override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val notification: Notification = getNotification()
        startForeground(1, notification)
        return START_NOT_STICKY
    }

    private fun getNotification(): Notification {
        val intent = Intent(this, MainActivity::class.java)
        val text = intent.getStringExtra("TITLE_NEWS")
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val notificationBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("SuperVipPro NewsApp")
            .setContentText("Add to mark list successfully!\n" +
                    "$text")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentIntent(pendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }

        return notificationBuilder.build()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }*/
}