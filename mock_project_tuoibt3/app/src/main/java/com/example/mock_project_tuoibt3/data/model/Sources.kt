package com.example.mock_project_tuoibt3.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import com.example.mock_project_tuoibt3.domain.model.Source
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "source")
data class Sources (
    /*@SerializedName("id")
    @Expose
    var id: String? = "",

    @PrimaryKey
    @SerializedName("name")
    var name: String = ""*/


    var id: String? = "",
    var name: String = ""
)