package com.example.mock_project_tuoibt3.ui.fragment.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LogInViewModel @Inject constructor(): ViewModel(){

    var isLoginSuccess  = MutableLiveData<Boolean>()
    private val firebaseAuth = FirebaseAuth.getInstance()

    fun getCurrentUser(): FirebaseUser? {
        return firebaseAuth.currentUser
    }
    fun doLogin(email: String, pass:String){
        firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener {
            isLoginSuccess.value = it.isSuccessful
        }
    }
}