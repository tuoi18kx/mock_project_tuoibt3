package com.example.mock_project_tuoibt3.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.OnItemClickListener
import com.example.mock_project_tuoibt3.R
import com.example.mock_project_tuoibt3.core.Topic

class TopicAdapter: RecyclerView.Adapter<TopicAdapter.TopicHolder> {

    private val TAG = "bnj"
    var context: Context
    var listTopic: List<Topic>
    var listener: OnItemClickListener

    constructor(
        context: Context,
        listTopic: List<Topic>,
        listener: OnItemClickListener
    ) : super() {
        this.context = context
        this.listTopic = listTopic
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicHolder {
        val itemView: View =
            LayoutInflater.from(context).inflate(R.layout.item_topic, parent, false)
        return TopicHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listTopic.size
    }

    override fun onBindViewHolder(holder: TopicHolder, position: Int) {
        Log.d(TAG, "onBindViewHolder: ard ch small")
        Glide.with(context)
            .load(listTopic[position].img)
            .into(holder.imv_topic)
        holder.txt_topic.text = listTopic[position].name
        holder.onClickItem(listener, position)
    }


    inner class TopicHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txt_topic: TextView
        val imv_topic: ImageView
        fun onClickItem(listener: OnItemClickListener, position: Int){
            itemView.setOnClickListener {
                listener.onItemClick(itemView, position)
            }
        }
        init {
            txt_topic = itemView.findViewById(R.id.txt_topic)
            imv_topic = itemView.findViewById(R.id.imv_topic)
        }
    }
}