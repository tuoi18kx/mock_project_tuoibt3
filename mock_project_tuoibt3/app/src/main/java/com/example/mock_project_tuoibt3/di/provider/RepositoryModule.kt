package com.example.mock_project_tuoibt3.di.provider

import com.example.mock_project_tuoibt3.data.local.dao.ArticlesDao
import com.example.mock_project_tuoibt3.data.remote.NewsApi
import com.example.mock_project_tuoibt3.data.repo.NewsRepositoryImpl
import com.example.mock_project_tuoibt3.domain.repo.NewsRepository
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    fun provideNewsRepository(apiService: NewsApi, articlesDao: ArticlesDao, firebaseDatabase: FirebaseDatabase) : NewsRepository {
        return NewsRepositoryImpl(apiService, articlesDao, firebaseDatabase)
    }
}