package com.example.mock_project_tuoibt3.ui.fragment.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.mock_project_tuoibt3.OnItemClickListener
import com.example.mock_project_tuoibt3.databinding.LayoutSearchBinding
import com.example.mock_project_tuoibt3.gone
import com.example.mock_project_tuoibt3.utils.imageloader.ImageLoader
import com.example.mock_project_tuoibt3.ui.adapter.NewsMatchWAdapter
import com.example.mock_project_tuoibt3.ui.main.MainActivity
import com.example.mock_project_tuoibt3.visible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : Fragment() {
    private lateinit var binding: LayoutSearchBinding
    private val viewModel: SearchViewModel by viewModels()
    @Inject
    lateinit var imgLoader: ImageLoader

    val args : SearchFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.observerNetwork((activity as MainActivity).isConnected)
        viewModel.getNewsByCategory("us", args.nameCate)

        if(args.isSearch){
            doSearch()
        }
        else{
            doDisplayListNews()
        }
    }
    private fun doDisplayListNews() {
        with(binding){
            edtSearch.gone()
            txtTitleCategory.visible()
            txtTitleCategory.text = args.nameCate
        }
        viewModel.listArticleByCategory.observe(viewLifecycleOwner){
            binding.rcvSearchNewsList.adapter = NewsMatchWAdapter(imgLoader, it, object : OnItemClickListener{
                override fun onItemClick(view: View, position: Int) {
                    val action =
                        SearchFragmentDirections.actionSearchFragment2ToArticlesWebViewFragment3(it[position])
                    findNavController().navigate(action)
                }
            })
        }
    }
    private fun doSearch() {
        with(binding){
            edtSearch.visible()
            txtTitleCategory.gone()
        }
        viewModel.listNewsSearch.observe(viewLifecycleOwner){
            binding.rcvSearchNewsList.adapter = NewsMatchWAdapter(imgLoader, it, object : OnItemClickListener{
                override fun onItemClick(view: View, position: Int) {
                    val action =
                        SearchFragmentDirections.actionSearchFragment2ToArticlesWebViewFragment3(it[position])
                    findNavController().navigate(action)
                }
            })
        }

        var job: Job? = null
        binding.edtSearch.addTextChangedListener {editable ->
            job?.cancel()
            job = MainScope().launch {
                editable?.let{
                    if(editable.toString().isNotEmpty()){
                        viewModel.getNewsSearch(keyWord = editable.toString())
                    }
                }
            }
        }
    }
}
