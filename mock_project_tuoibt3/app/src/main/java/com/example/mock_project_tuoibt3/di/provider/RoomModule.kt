package com.example.mock_project_tuoibt3.di.provider

import android.content.Context
import androidx.room.Room
import com.example.mock_project_tuoibt3.data.local.dao.ArticlesDao
import com.example.mock_project_tuoibt3.data.local.db.ArticlesDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun provideDB(@ApplicationContext context: Context): ArticlesDatabase{
        return Room.databaseBuilder(
            context,
            ArticlesDatabase::class.java,
            "articles_database"
        ).allowMainThreadQueries().build()
    }


    @Singleton
    @Provides
    fun getDao(articlesDB: ArticlesDatabase): ArticlesDao{
        return articlesDB.articlesDao()
    }
}