package com.example.mock_project_tuoibt3.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mock_project_tuoibt3.OnItemClickListener
import com.example.mock_project_tuoibt3.databinding.ItemNewsSmallBinding
import com.example.mock_project_tuoibt3.domain.model.Article

class NewsSmallAdapter(private val listNews: List<Article>, private val listener: OnItemClickListener) :
    RecyclerView.Adapter<NewsSmallAdapter.NewsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = ItemNewsSmallBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsViewHolder(binding)
    }
    override fun getItemCount(): Int {
        return 4
    }
    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        Glide.with(holder.binding.root)
            .load(listNews[position].urlToImage)
            .into(holder.binding.imvNewsThumbnail)
        holder.binding.txtNewsTitle.text = listNews[position].title
        holder.onClickItem(listener, position)
    }
    inner class NewsViewHolder(val binding: ItemNewsSmallBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onClickItem(listener: OnItemClickListener, position: Int) {
            itemView.setOnClickListener {
                listener.onItemClick(itemView, position)
            }
        }
    }
}