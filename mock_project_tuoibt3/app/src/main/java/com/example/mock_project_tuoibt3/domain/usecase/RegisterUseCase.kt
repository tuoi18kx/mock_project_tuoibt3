package com.example.mock_project_tuoibt3.domain.usecase

import com.example.mock_project_tuoibt3.domain.repo.FirebaseRepository
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val firebaseRepository: FirebaseRepository)
{
    suspend fun doLogin(email: String, pass:String) {
        firebaseRepository.doLogin(email, pass)
    }

    suspend fun doRegister(email: String, pass: String) {
        firebaseRepository.doRegister(email, pass)
    }

    fun doLogout(){
        firebaseRepository.doLogout()
    }
}