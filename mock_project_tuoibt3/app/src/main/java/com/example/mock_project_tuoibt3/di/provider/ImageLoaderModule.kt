package com.example.mock_project_tuoibt3.di.provider

import android.content.Context
import com.example.mock_project_tuoibt3.utils.imageloader.GlideImageLoader
import com.example.mock_project_tuoibt3.utils.imageloader.ImageLoader
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ImageLoaderModule {
    @Provides
    @Singleton
    fun provideGlideImageLoader(@ApplicationContext context: Context): ImageLoader {
        return GlideImageLoader(context)
    }
}